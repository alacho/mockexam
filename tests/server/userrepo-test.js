
const {createUser, getUser ,removeUser} = require('../../src/server/db/userRepo.js');

describe("UserRepo", () => {
  it("Should delete a user from the repo", () => {
    createUser("Håvard", 1234);
    expect(getUser("Håvard")).toBeDefined();

    removeUser("Håvard");
    expect(getUser("Håvard")).toEqual(undefined)
  });
});