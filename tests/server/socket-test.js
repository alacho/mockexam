const request = require('supertest');
const {app} = require('../../src/server/app');
const WS = require('ws');

const {asyncCheckCondition, checkConnectedWS} = require('../mytest-util.js');

let server;
let port;

describe("WebSocket Handling",  () => {
  beforeAll(done => {
    server = app.listen(0, ()=> {
      port = server.address().port;
      done();
    });
  });

  afterAll(() => {
    server.close();
  });

  const sockets = [];

  afterEach(() => {
    /*
        make sure to manually free the sockets, otherwise might block Jest and the
        shutting down of Express...
    */
    for(let i=0; i<sockets.length; i++){
      console.log("Closing socket: " + i);
      sockets[i].close();
    }
    sockets.length = 0;
  });

  it("Should accept a connection",  async () => {
    const first = new WS(`ws://localhost:${port}`);
    sockets.push(first);
    let connected = await checkConnectedWS(first, 2000);
    expect(connected).toBe(true);
  });

  it("Should handle many connections", async () => {
    for(let i = 1; i < 20; i++){
      const connection = new WS(`ws://localhost:${port}`);
      sockets.push(connection);
      let connected = await checkConnectedWS(connection, 2000);
      expect(connected).toBe(true);
      expect(sockets.length).toBe(i)
    }
  });

  it("Should distribute number of clients",  async () => {
    const first = new WS(`ws://localhost:${port}`);
    sockets.push(first);
    first.on('message', event => {
      const message = JSON.parse(event);
      expect(message.noClient).toBeGreaterThanOrEqual(1)
    });
    await checkConnectedWS(first, 2000);
  });

});
