const request = require('supertest');
const {app} = require('../../src/server/app.js');
const repo = require('../../src/server/middleware/menuHandling.js');

describe("Testing the menu api", () => {
  it("Should get one menu", async () => {
    const response = await request(app).get('/api/menu/1');
    expect(response.body.id).toBe("1");
    expect(response.status).toBe(200);
  });

  it("Should test for non-existent menu", async () => {
    const response = await request(app).get('/api/menu/-3');
    expect(response.statusCode).toBe(404);
  });

  it("Should do post request", async () => {
    const response = await request(app).post("/api/menu")
      .send({days: ["Kake", "Banan", "Sjokolade", "Egg", "Kebab"]})
      .set('Content-Type', 'application/json');

    expect(response.statusCode).toBe(201);
  });

  /*it("Should update a menu through put", async () => {
    const getResponse = await request(app).get("/api/menu/1");
    expect(getResponse.body.days[0].meal).toBe("Taco");

    const response = await request(app).put("/api/menu/1")
      .send({id: 1, days: [{name: "Monday", meal: "Kebab"}]})
      .set('Content-Type', 'application/json');

    console.log(response);
  });*/
});