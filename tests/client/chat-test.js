const React = require('react');
const {mount} = require('enzyme');

const WS = require('ws');

const {overrideWebSocket,
       overrideFetch,
       asyncCheckCondition} = require('../mytest-util.js');

const {Chat} = require('../../src/client/components/Chat.jsx');
const {app} = require('../../src/server/app.js');

let server;
let port;

describe("Testing the Chat component", () => {
  beforeAll(done => {
    server = app.listen(0, ()=> {
      port = server.address().port;
      done();
    });
  });

  afterAll(() => {
    server.close();
  });

  it("Should compose a basic test", () => {
    expect(true).toBe(true);
  });

  it("Test new message", async () => {

    overrideFetch(app);
    overrideWebSocket(port);

    const driver = mount(<Chat/>);

    const msg = "Hello!";

    const predicate = () => {
      driver.update();
      const html = driver.html();
      return html.includes(msg);
    };

    let displayedMessage;

    const nameInput = driver.find('#inputName').at(0);
    const msgInput = driver.find('#messageArea').at(0);
    const sendBtn = driver.find('#sendBtn').at(0);

    nameInput.simulate('change', {target: {value: "foo"}});
    msgInput.simulate('change', {target: {value: msg}});
    sendBtn.simulate('click');

    displayedMessage = await asyncCheckCondition(predicate, 3000, 100);
    expect(displayedMessage).toBe(true);
  });
});