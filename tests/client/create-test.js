const {MemoryRouter} = require("react-router-dom");
const React = require('react');

const {overrideFetch} = require("../mytest-util.js");
const {app} = require('../../src/server/app.js');

const {Create} = require('../../src/client/components/Create.jsx');

const {mount} = require('enzyme');

function fillForm(driver, dishArray){
  const dayArray = ["monday", "tuesday", "wednesday", "thursday", "friday"];

  dayArray.forEach((day, index) => {
    const dayInput = driver.find(`#${day}Input`).at(0);
    dayInput.simulate('change', {target: {value: dishArray[index]}})
  });

  const btn = driver.find("#btn").first();
  btn.simulate('click');
}


describe("Testing the create component", () => {
  it("Should mount the component and fill the form", async () => {
    overrideFetch(app);
    let page = null;
    const history = {push: (h) => {page=h}};
    const driver = mount(<MemoryRouter initialEntries={["/create"]}>
      <Create username="Håvard" history={history}/>
    </MemoryRouter>);

    fillForm(driver, ["Taco", "Kebab", "Pita", "Lok", "Potet"]);
    expect(driver.html().includes("Taco" && "Potet")).toBe(true);
  });

});