const {asyncCheckCondition, stubFetch, overrideFetch} = require( "../mytest-util.js");
const React = require('react');
const {mount} = require("enzyme");
const {MemoryRouter} = require( "react-router-dom");
const {Edit} = require("../../src/client/components/Edit.jsx");
const {app} = require('../../src/server/app.js');

function fillForm(driver, dishArray) {

  const dayArray = ["Monday"];

  dayArray.forEach((day, index) => {
    const dayInput = driver.find(`#${day}Input`).at(0);
    console.log(`#${day}Input`);

    dayInput.simulate('change', {target: {value: dishArray[index]}})
  });
}

describe("Testing Edit component", () => {
  test("Should mount Edit component and click the button", async () => {
    stubFetch(200,
      {id: 1, days: [
        {name: "Monday", meal: "Banan"},
        ]},
      (url) => url.endsWith("/api/books/1"));

    let page = null;
    const history = {push: (h) => {page=h}};

    const driver = mount(
      <MemoryRouter initialEntries={["/edit"]}>
        <Edit username="Håvard" history={history}/>
      </MemoryRouter>);

    driver.find("#btn").first().simulate('click');
    const mondayText = await asyncCheckCondition(
      () => {driver.update(); return driver.html().includes("Monday")},
      2000 ,200);
    fillForm(driver, ["Banan", "Kake", "Sjokolade", "Batman", "Nanana"]);

    expect(mondayText).toBe(true);
    expect(driver.html().includes("Banan")).toBe(true);

    //expect(driver.html().includes("")).toBe(true);
  });

});