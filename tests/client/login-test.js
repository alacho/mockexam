const React = require('react');
const {mount} = require('enzyme');
const {MemoryRouter} = require('react-router-dom');
const {app} = require('../../src/server/app.js');

const {resetAllUsers, getUser, createUser} = require('../../src/server/db/userRepo.js');

const {overrideFetch, asyncCheckCondition} = require('../mytest-util.js');

const {Login} = require('../../src/client/components/Login.jsx');

function fillForm(driver, id, password){

  const userIdInput = driver.find("#username").at(0);
  const passwordInput = driver.find("#password").at(0);
  const loginBtn = driver.find("#btn").at(0);

  userIdInput.simulate('change', {target: {value: id}});
  passwordInput.simulate('change', {target: {value: password}});
  loginBtn.simulate('click');
}

describe("Testing the Login component", () => {
  it("Should check for login to have error", async () => {
    overrideFetch(app);
    const driver = mount(
      <MemoryRouter>
        <Login />
      </MemoryRouter>);

    fillForm(driver, "Håvard", "1234");
    const error = await asyncCheckCondition(
      () => {driver.update(); return driver.html().includes("Invalid username/password")},
      2000 ,200);

    expect(error).toEqual(true);
  });

  it("Should test for valid login", async () =>{

    const userId = "Foo";
    const password = "123";
    createUser(userId, password);

    overrideFetch(app);

    const fetchAndUpdateUserInfo = () => new Promise(resolve => resolve());
    let page = null;
    const history = {push: (h) => {page=h}};

    const driver = mount(
      <MemoryRouter initialEntries={["/login"]}>
        <Login setSignIn={fetchAndUpdateUserInfo} history={history} />
      </MemoryRouter>
    );
    fillForm(driver, userId, password);

    const redirected = await asyncCheckCondition(
      () => {return page === "/"},
      2000 ,200);
    expect(redirected).toEqual(true);
  });

});