import {overrideFetch, asyncCheckCondition} from "../mytest-util.js";

const React = require('react');
const {mount} = require('enzyme');
const {BrowserRouter, MemoryRouter} = require('react-router-dom');

const {Home} = require('../../src/client/components/Home.jsx');

const {app} = require('../../src/server/app.js');

describe("Testing the Home component", () => {
  it("Should test for home text", () => {
    overrideFetch(app);
    const driver = mount(
      <BrowserRouter>
        <Home />
      </BrowserRouter>
    );

    const html = driver.html();
    expect(html.includes("Hello")).toBe(true)
  });

  it("Should test for for signed in username text", async () => {
    overrideFetch(app);

    const user = "Håvard";
    const fetchAndUpdateUserInfo = () => new Promise(resolve => resolve());

    const driver = mount(
      <BrowserRouter initialEntries={["/home"]}>
        <Home fetchAndUpdateUserInfo={fetchAndUpdateUserInfo} username={user}/>
      </BrowserRouter>
    );

    const html = driver.html();
    expect(html.includes("Håvard")).toEqual(true);
  });

  it("Should display the menu", async () => {
    overrideFetch(app);
    const fetchAndUpdateUserInfo = () => new Promise(resolve => resolve());

    const driver = mount(
      <BrowserRouter initialEntries={["/home"]}>
        <Home fetchAndUpdateUserInfo={fetchAndUpdateUserInfo} />
      </BrowserRouter>
    );

    const predicate = () => {
      driver.update();
      const html = driver.html();
      return html.includes("Monday"
        && "Tuesday"
        && "Wednesday"
        && "Thursday"
        && "Friday") ;
    };
    //await driver.update();

    let displayedMessage = await asyncCheckCondition(predicate, 3000, 100);
    expect(displayedMessage).toEqual(true);
  });
});