import React from 'react';
import {getMenu} from "../client-util";
//import {getMenu} from "../client-util.js";

export class Edit extends React.Component {

  state = {
    id: 0,
    menu: [],
    errorMsg: null,
  };

  componentDidMount(){
    const paramMap = new Map();
    const searchSplit = location.search.split("&");
    searchSplit.forEach(item => {
      paramMap.set(item.split("=")[0], item.split("=")[1])
    });

    this.collectMenu(paramMap.get("?menuId"))

    /*for (let [key, value] of paramMap.entries()) {
      console.log(key + ' = ' + value);
    } */
  }

  collectMenu = async (id) => {
    const res = await getMenu(id);
    if(res.status !== 200) {
      window.location = "/create";
      return
    }
    this.setState({id: res.menu.id, menu: res.menu})
  };

  changeMenu = async () => {
    const id = this.state.id;
    const menu = this.state.menu;

    const url =`/api/menu/${id}`;

    const payload = menu;

    let response;
    try {
      response = await fetch(url, {
        method: "put",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(payload)
      })
    } catch(err) {
      return false
    }
    this.props.history.push("/");
    return response.status === 204
  };

  onItemChange = (n, e) => {
    const days = [...this.state.menu.days];
    days[n].meal = e.target.value;
    this.setState({days: days})
  };

  render() {
    console.log(this.state.menu);
    const days = this.state.menu.days;
    return (
      <div>
        <h2>Edit</h2>
        {days && days.map((item, index) => {
          return (
            <div key={item.name}>
              <label htmlFor={item.name}>{item.name}</label>
              <input name={item.name}
                     id={`${item.name}Input`}
                     placeholder={item.meal}
                     onChange={(event) => this.onItemChange(index, event)} />
            </div>
          )
        })}
        <div id="btn" className="text-primary" onClick={this.changeMenu}>Change menu</div>
      </div>
    )
  }
}

export default Edit