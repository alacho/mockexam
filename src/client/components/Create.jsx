import React from 'react';

const dayArray = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

export class Create extends React.Component {

  state = {
    days: ["", "", "", "", ""], //Days Monday - Friday
  };

  createMenu = async () => {
    const url = "/api/menu";

    const payload = {days: this.state.days};

    let response;
    try {
      response = await fetch(url, {
        method: "post",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(payload)
      })
    } catch(err) {
      return false
    }
    this.props.history.push("/");
    return response.status === 201
  };

  onItemChange = (n, e) => {
    const days = [...this.state.days];
    days[n] = e.target.value;

    this.setState({days: days})
  };

  render() {
    return (
      <div>
        <h2>Create a new menu</h2>
        {dayArray.map((item, index) => {
          const inputName = item.toLowerCase();
          return (
            <div key={item}>
              <label htmlFor={item}>{item}</label>
              <input name={item}
                     id={`${inputName}Input`}
                     value={this.state.days[index]}
                     onChange={(event) => {this.onItemChange(index, event)}}/>
            </div>
          )
        })}
        <div id="btn" className="text-primary" onClick={this.createMenu}>Create menu</div>
      </div>
    )
  }
}

export default Create